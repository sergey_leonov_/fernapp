package com.fernapp.server.executor.X11;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

/**
 * @author Markus
 * 
 */
public class KeycodeConverter {

	private static final Logger log = LoggerFactory.getLogger(KeycodeConverter.class);

	private static Map<JavaKeyEvent, Long> keyEventToKeysym;

	static {
		// add entries from keysym2UCSHash first, then add keysym2JavaKeycodes.
		// if they point to the same keysym create permutations

		Map<Long, JavaKeyEvent> keysymToKeyEventMap = new HashMap<Long, KeycodeConverter.JavaKeyEvent>();

		// keyChar
		for (Entry<Long, Character> e : XKeysym.keysym2UCSHash.entrySet()) {
			Long keysym = e.getKey();
			JavaKeyEvent javaKeyEvent = new JavaKeyEvent(null, e.getValue(), null);
			Preconditions.checkState(!keysymToKeyEventMap.containsKey(keysym));
			keysymToKeyEventMap.put(keysym, javaKeyEvent);
		}

		// keyCode, keyLocation
		for (Entry<Long, XKeysym.Keysym2JavaKeycode> e : XKeysym.keysym2JavaKeycodeHash.entrySet()) {
			Long keysym = e.getKey();
			JavaKeyEvent javaKeyEvent = keysymToKeyEventMap.get(keysym);

			if (javaKeyEvent == null) {
				// doesn't exist yet
				javaKeyEvent = new JavaKeyEvent(null, KeyEvent.CHAR_UNDEFINED, null);
			} else if (javaKeyEvent.getKeyCode() == null && javaKeyEvent.getKeyLocation() == null) {
				// just add additional information (but remove it first so we don't break
				// the hashmap)
				keysymToKeyEventMap.remove(keysym);
			} else {
				// copy it to create a permutation
				javaKeyEvent = new JavaKeyEvent(null, javaKeyEvent.getKeyChar(), null);
			}

			javaKeyEvent.setKeyCode(e.getValue().getJavaKeycode());
			javaKeyEvent.setKeyLocation(e.getValue().getKeyLocation());

			Preconditions.checkState(!keysymToKeyEventMap.containsKey(keysym));
			keysymToKeyEventMap.put(keysym, javaKeyEvent);
		}

		// modifications:
		// \n maps to XK_Return and XK_Linefeed and we need XK_Return
		keysymToKeyEventMap.remove(XKeySymConstants.XK_Linefeed);
		// AltGr maps to XK_ISO_Level3_Shift and XK_Mode_switch. We need
		// XK_ISO_Level3_Shift
		keysymToKeyEventMap.remove(XKeySymConstants.XK_Mode_switch);
		keysymToKeyEventMap.remove(XKeySymConstants.XK_decimalpoint);

		// create reverse keyEvent->keysym map
		keyEventToKeysym = new HashMap<KeycodeConverter.JavaKeyEvent, Long>();
		for (Entry<Long, JavaKeyEvent> e : keysymToKeyEventMap.entrySet()) {
			Long keysym = e.getKey();
			if (keysym < 0x10000000) {

				JavaKeyEvent javaKeyEvent = e.getValue();
				Long existingKeysym = keyEventToKeysym.get(javaKeyEvent);
				if (existingKeysym != null) {
					if (log.isTraceEnabled()) {
						log.trace("KeyEvent " + javaKeyEvent.toString() + " maps to keysym 0x"
								+ Long.toHexString(existingKeysym) + " and 0x" + Long.toHexString(keysym));
					}
					// we'll to use the smaller keysym (usually a more important keysym)
					if (keysym < existingKeysym) {
						keyEventToKeysym.put(javaKeyEvent, keysym);
					}
				} else {
					keyEventToKeysym.put(javaKeyEvent, keysym);
				}
			}
		}
	}

	/**
	 * Converts the java keycode to the corresponding X11 keysym code.
	 */
	public static long getKeysym(int keyCode, char keyChar, int keyLocation) {

		// keysym can be mapped to the keyCode/keyLocation/keyChar tuple.
		// better to do this lookup first because its based on more information (for
		// instance, standard key or keypad)

		// lookup with all three parts
		{
			JavaKeyEvent javaKeyEvent = new JavaKeyEvent(keyCode, keyChar, keyLocation);
			Long keysym = keyEventToKeysym.get(javaKeyEvent);
			if (keysym != null) {
				return keysym;
			}
		}

		// only keyChar (keyChar usually is more precise than keyCode/keyLocation, i.e.
		// EuroSymbol)
		{
			JavaKeyEvent javaKeyEvent = new JavaKeyEvent(null, keyChar, null);
			Long keysym = keyEventToKeysym.get(javaKeyEvent);
			if (keysym != null) {
				return keysym;
			}
		}

		{
			JavaKeyEvent javaKeyEvent = new JavaKeyEvent(keyCode, KeyEvent.CHAR_UNDEFINED, keyLocation);
			Long keysym = keyEventToKeysym.get(javaKeyEvent);
			if (keysym != null) {
				return keysym;
			}
		}

		if (keyChar != KeyEvent.CHAR_UNDEFINED) {
			keyChar = Character.toLowerCase(keyChar);

			// any Unicode/ISO 10646 character in the range U0100 to U10FFFF can be
			// represented by a keysym value in the range 0x01000100 to 0x0110FFFF. The
			// Latin-1 characters in the first row of ISO 10646 (U0000 to U00FF) are
			// already represented by keysyms with the same value

			// direct conversion of unicode keyChar to keysym. ()
			if (keyChar >= 0x00000110 && keyChar <= 0x0010FFFF) {
				long keysym = 0x01000000 | keyChar;
				return keysym;
			}

			// Latin-1 characters (1:1 mapping)
			if ((keyChar >= 0x0020 && keyChar <= 0x007e) || (keyChar >= 0x00a0 && keyChar <= 0x00ff)) {
				return keyChar;
			}
		}

		throw new IllegalArgumentException("Unable to convert keyCode=" + keyCode + ", keyChar=" + keyChar
				+ ", keyLocation=" + keyLocation + " to X11 keysym");
	}

	private static class JavaKeyEvent {
		private Integer keyCode;
		private Character keyChar;
		private Integer keyLocation;

		private JavaKeyEvent(Integer keyCode, Character keyChar, Integer keyLocation) {
			this.keyCode = keyCode;
			this.keyChar = keyChar;
			this.keyLocation = keyLocation;
		}

		@Override
		public String toString() {
			return "JavaKeyEvent [keyCode=" + keyCode + ", keyChar=" + keyChar + ", keyLocation=" + keyLocation + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((keyChar == null) ? 0 : keyChar.hashCode());
			result = prime * result + ((keyCode == null) ? 0 : keyCode.hashCode());
			result = prime * result + ((keyLocation == null) ? 0 : keyLocation.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			JavaKeyEvent other = (JavaKeyEvent) obj;
			if (keyChar == null) {
				if (other.keyChar != null)
					return false;
			} else if (!keyChar.equals(other.keyChar))
				return false;
			if (keyCode == null) {
				if (other.keyCode != null)
					return false;
			} else if (!keyCode.equals(other.keyCode))
				return false;
			if (keyLocation == null) {
				if (other.keyLocation != null)
					return false;
			} else if (!keyLocation.equals(other.keyLocation))
				return false;
			return true;
		}

		public Integer getKeyCode() {
			return keyCode;
		}

		public void setKeyCode(Integer keyCode) {
			this.keyCode = keyCode;
		}

		public Character getKeyChar() {
			return keyChar;
		}

		public Integer getKeyLocation() {
			return keyLocation;
		}

		public void setKeyLocation(Integer keyLocation) {
			this.keyLocation = keyLocation;
		}

	}

}
