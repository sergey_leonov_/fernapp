package com.fernapp.server.encoding;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.google.common.base.Stopwatch;

/**
 * A simple abstract implementation of {@link Encoder} which only does on-demand compression, adds thead-safety and
 * delegates to native methods for the hard work.
 * 
 * @author Markus
 */
public abstract class AbstractNativeEncoder implements Encoder {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private final String windowId;
	private final WindowContentProvider windowContentProvider;
	private final MeasurementReceiver measurementReceiver;
	/**
	 * A pointer in the C address space. Used by the native encoder as context to store arbitrary data.
	 */
	private final long encoderContext;
	private volatile boolean initialized;

	private final Object updateLock = new Object();
	private Long timeOfOldestUpdateNotification;
	private DamageReport mergedDamageReport;

	private final Object encodingLock = new Object();

	/**
	 * @param windowContentProvider
	 */
	public AbstractNativeEncoder(String windowId, WindowContentProvider windowContentProvider,
			MeasurementReceiver measurementReceiver) {
		this.windowId = windowId;
		this.windowContentProvider = windowContentProvider;
		this.measurementReceiver = measurementReceiver;
		encoderContext = _init();
		initialized = true;
		timeOfOldestUpdateNotification = System.nanoTime();
	}

	public String getWindowId() {
		return windowId;
	}

	public final void onWindowUpdated(DamageReport damageReport) {
		synchronized (updateLock) {
			// we need the oldest timestamp - so only change it if its not yet set
			if (timeOfOldestUpdateNotification == null) {
				timeOfOldestUpdateNotification = System.nanoTime();
			}

			if (mergedDamageReport == null) {
				mergedDamageReport = damageReport;
			} else {
				mergedDamageReport = mergedDamageReport.mergeWith(damageReport);
			}
		}
	}

	public final VideoStreamChunk getEncodedChunk() {
		VideoStreamChunk chunk = null;
		synchronized (encodingLock) {
			if (initialized) {
				// atomically get the value and change it to null
				Long oldTimeOfOldestUpdateNotification;
				DamageReport damageReport;
				synchronized (updateLock) {
					oldTimeOfOldestUpdateNotification = timeOfOldestUpdateNotification;
					damageReport = mergedDamageReport;
					timeOfOldestUpdateNotification = null;
					mergedDamageReport = null;
				}

				if (oldTimeOfOldestUpdateNotification != null) {
					long waitTimeNanos = System.nanoTime() - oldTimeOfOldestUpdateNotification;
					long waitTimeMillis = TimeUnit.MILLISECONDS.convert(waitTimeNanos, TimeUnit.NANOSECONDS);
					measurementReceiver.receiveMeasurement(DelayCategory.WAIT, waitTimeMillis, 0);

					// do the encoding
					WindowContent windowContent = windowContentProvider.getWindowContent();
					if (windowContent != null) {
						long windowContentPointer = windowContent.getNativePointer();
						Stopwatch stopwatch = new Stopwatch().start();
						chunk = _encode(windowContentPointer, damageReport, encoderContext);
						stopwatch.stop();
						measurementReceiver.receiveMeasurement(DelayCategory.ENCODE, stopwatch.elapsedMillis(),
								chunk.getData().length);
					} else {
						log.debug("Content provider did not return anything. Window is probably gone.");
					}
				}
			} else {
				log.debug("Encoder has no update because it was shutted down");
			}
		}
		return chunk;
	}

	public final void changeQuality(int quality) {
		synchronized (encodingLock) {
			if (!initialized) {
				throw new IllegalStateException("Encoder is no longer initialized");
			}
			_changeQuality(quality, encoderContext);
		}
	}

	public final void shutdown() {
		synchronized (encodingLock) {
			_shutdown(encoderContext);
			initialized = false;
		}
	}

	@Override
	public String toString() {
		return "Encoder for window " + windowId;
	}

	@Override
	protected void finalize() throws Throwable {
		if (initialized) {
			log.error("Shutdown has not been called!");
		}
	}

	/** Encoder initialization. Returns a pointer to its context. */
	protected abstract long _init();

	protected abstract VideoStreamChunk _encode(long windowContentPointer, DamageReport damageReport, long encContext);

	protected abstract void _changeQuality(int quality, long encContext);

	protected abstract void _shutdown(long encContext);

}
