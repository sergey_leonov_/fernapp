package com.fernapp.server.encoding;

import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.uacommon.measurement.MeasurementReceiver;

/**
 * @author Markus
 * 
 */
public class H264Encoder extends AbstractNativeEncoder {

	public H264Encoder(String windowId, WindowContentProvider windowContentProvider,
			MeasurementReceiver measurementReceiver) {
		super(windowId, windowContentProvider, measurementReceiver);
	}

	protected native long _init();

	protected native VideoStreamChunk _encode(long windowContentPointer, DamageReport damageReport, long encContext);

	protected native void _changeQuality(int quality, long encContext);

	protected native void _shutdown(long encContext);

}
