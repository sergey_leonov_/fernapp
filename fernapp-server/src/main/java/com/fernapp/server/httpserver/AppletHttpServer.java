package com.fernapp.server.httpserver;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Markus
 */
public class AppletHttpServer {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private final int port;

	public AppletHttpServer(int port) {
		this.port = port;
	}

	public void init() {
		try {
			String rootPath = "./web";

			Server server = new Server(port);

			ResourceHandler resource_handler = new ResourceHandler();
			resource_handler.setResourceBase(rootPath);
			resource_handler.setDirectoriesListed(true);
			resource_handler.setWelcomeFiles(new String[] { "index.html" });

			HandlerList handlers = new HandlerList();
			handlers.setHandlers(new Handler[] { resource_handler, new DefaultHandler() });
			server.setHandler(handlers);

			server.start();
		} catch (Exception e) {
			log.error("Failed to start HTTP server", e);
		}

		String s = "\n\n";
		s += "=========================================================================\n";
		s += "   Java applet client is available at http://localhost:" + port + "/index.html\n";
		s += "=========================================================================\n";
		log.info(s);
	}

}
