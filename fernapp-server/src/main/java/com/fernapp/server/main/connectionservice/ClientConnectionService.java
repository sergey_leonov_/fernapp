package com.fernapp.server.main.connectionservice;

import com.fernapp.uacommon.middleware.MessagingConnection;

/**
 * Encapsulates access to the transportation layer.
 */
public interface ClientConnectionService {

	void start();

	void stop();

	void setConnectListener(ConnectListener listener);

	public interface ConnectListener {
		/**
		 * Called when a new {@link MessagingConnection} has been established, which is now ready to be used by the
		 * application.
		 */
		void onConnected(MessagingConnection connection);
	}

}
