package com.fernapp.server.main.connectionservice;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.uacommon.middleware.channel.SocketFactory;
import com.google.common.base.Preconditions;

public class TcpClientConnectionService implements ClientConnectionService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private final ServerSocket serverSocket;
	private final AtomicInteger nextRaupConnectionId = new AtomicInteger(0);
	private volatile ConnectListener listener;
	private boolean active = false;

	public TcpClientConnectionService(int port) {
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void start() {
		Preconditions.checkState(!active);
		active = true;

		log.info("Starting to accept connections on " + serverSocket.toString());

		Thread socketAcceptThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					acceptConnections();
				} catch (Exception e) {
					log.error("Socket accept thread failed", e);
				}
			}
		});
		socketAcceptThread.start();
	}

	@Override
	public synchronized void stop() {
		Preconditions.checkState(active);
		active = false;

		log.info("Stopping to accept connections on " + serverSocket.toString());
		try {
			serverSocket.close();
		} catch (Exception e) {
			log.warn("Failed to close socket", e);
		}
	}

	private void acceptConnections() throws IOException {
		while (active) {
			try {
				Socket socket = null;
				try {
					socket = serverSocket.accept();
					SocketFactory.configureServerSocket(socket);

					int raupConnectionId = nextRaupConnectionId.getAndIncrement();
					String connectionName = "raup" + raupConnectionId;
					DataChannel dataChannel = new SocketDataChannel(socket);
					MessagingConnection messagingConnection = new JavaSerMessagingConnection(dataChannel,
							connectionName);

					listener.onConnected(messagingConnection);
				} catch (SocketException e) {
					if (active) {
						log.error("Socket accept failed while service was active", e);
					} else {
						log.info("Socket accept stopped. Service has been is disabled.");
					}
				}
			} catch (IOException e) {
				log.error("IOException while establishing connection with RAUP client", e);
			}
		}
	}

	@Override
	public void setConnectListener(ConnectListener listener) {
		this.listener = listener;
	}

}
