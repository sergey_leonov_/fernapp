package com.fernapp.server.main;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.server.httpserver.AppletHttpServer;
import com.fernapp.server.raupserver.AnalyticsCollector;
import com.fernapp.server.raupserver.DefaultServerRaupConnection;
import com.fernapp.server.raupserver.RaupServer;

public class FernappServer {

	private final static Logger log = LoggerFactory.getLogger(FernappServer.class);

	public static void main(String[] applicationCommand) {
		try {
			Thread.currentThread().setName("server-main");
			FernappServerOptions options = loadOptions();

			DefaultServerRaupConnection.passphrase = options.getPassphrase();

			AnalyticsCollector analyticsCollector = new AnalyticsCollector(options.isAnalyticsEnabled());
			RaupServer raupServer = new RaupServer(options.getTcpRaupPort(), options.getRuntimeDir(), applicationCommand,
					analyticsCollector);
			raupServer.start();

			new AppletHttpServer(options.getHttpPort()).init();
		} catch (Exception e) {
			log.error("Fernapp server failed to start", e);
		}
	}

	/**
	 * Load JVM system properties into {@link FernappServerOptions}.
	 */
	private static FernappServerOptions loadOptions() {
		FernappServerOptions options = new FernappServerOptions();

		for (Field field : options.getClass().getDeclaredFields()) {
			field.setAccessible(true);

			String propertyName = "fernapp." + field.getName();
			String propertyValue = System.getProperty(propertyName);
			if (propertyValue != null) {
				try {
					if (field.getType().isAssignableFrom(int.class)) {
						field.set(options, Integer.parseInt(propertyValue));
					} else if (field.getType().isAssignableFrom(boolean.class)) {
						field.set(options, Boolean.parseBoolean(propertyValue));
					} else if (field.getType().isAssignableFrom(String.class)) {
						field.set(options, propertyValue);
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}

		return options;
	}

}
