package com.fernapp.server.quality;

import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.quality.DummyData;
import com.fernapp.raup.quality.EchoRequest;
import com.fernapp.raup.quality.EchoResponse;
import com.fernapp.raup.quality.ReceiveReport;
import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.LoopbackConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.util.Callback;
import com.google.common.base.Stopwatch;

/**
 * @author Markus
 * 
 */
public class QualityFeedbackServiceTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private MessagingConnection clientConnection;

	/**
	 * Sends 50KB test data over a messaging connection to verify the QoS measurements.
	 */
	@Test
	public void testMeasurement() throws Exception {
		final LoopbackConnection lp = new LoopbackConnection();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientConnection = new JavaSerMessagingConnection(new SocketDataChannel(lp.getClientSocket()),
							"clientMc");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.setName("ClientMcConnect");
		clientConnectThread.start();
		MessagingConnection serverConnection = new JavaSerMessagingConnection(new SocketDataChannel(
				lp.getServerSocket()), "serverMc");
		clientConnectThread.join();
		Assert.assertTrue(serverConnection.isOpen());
		Assert.assertTrue(clientConnection.isOpen());

		// packet delay
		clientConnection.registerReceivedHandler(EchoRequest.class, new Callback<EchoRequest>() {
			public void onCallback(EchoRequest payload) throws Exception {
				clientConnection.sendMessage(new EchoResponse());
			}
		});

		// WindowContentUpdate responser
		clientConnection.registerReceivedHandler(WindowContentUpdate.class, new Callback<WindowContentUpdate>() {
			public void onCallback(WindowContentUpdate payload) throws Exception {
				ReceiveReport receiveReport = new ReceiveReport(payload.getServerSendTime());
				clientConnection.sendMessage(receiveReport);
			}
		});

		// dummy data
		clientConnection.registerReceivedHandler(DummyData.class, new Callback<DummyData>() {
			public void onCallback(DummyData payload) {
				// discard
			}
		});

		// QoS
		final QualityFeedbackService qualityFeedbackService = new QualityFeedbackService();
		serverConnection.registerReceivedHandler(ReceiveReport.class, new Callback<ReceiveReport>() {
			public void onCallback(ReceiveReport payload) {
				qualityFeedbackService.handleReceiveReport(payload);
			}
		});

		clientConnection.startReceivingMessages();
		serverConnection.startReceivingMessages();

		qualityFeedbackService.connectionWarmUp(serverConnection);

		// test QoS
		qualityFeedbackService.determinePacketDelay(serverConnection);
		int dataSize = 100000;
		int repetitions = 5;
		long endToEndDelaySum = 0;
		for (int i = 0; i < repetitions; i++) {
			log.info("Sending test message");
			qualityFeedbackService.lastUpdateEndToEndDelay = null;
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.start();
			WindowContentUpdate windowContentUpdate = new WindowContentUpdate("123", new VideoStreamChunk(0, 0, 0,
					false, new byte[dataSize]));
			qualityFeedbackService.prepareForQoS(windowContentUpdate);
			serverConnection.sendMessage(windowContentUpdate);
			while (qualityFeedbackService.lastUpdateEndToEndDelay == null) {
				TimeUnit.MILLISECONDS.sleep(5);
			}
			stopwatch.stop();
			log.info("Control measurement: " + stopwatch.elapsedMillis());
			Assert.assertTrue(qualityFeedbackService.lastUpdateEndToEndDelay <= stopwatch.elapsedMillis());
			endToEndDelaySum += qualityFeedbackService.lastUpdateEndToEndDelay;
		}

		// verify
		long avgEndToEndDelay = endToEndDelaySum / repetitions;
		// expectedDelay should be close to the packet delay (loopback bandwidth is huge)
		long expectedDelay = qualityFeedbackService.getPacketDelay();
		long delta = Math.abs(avgEndToEndDelay - expectedDelay);
		log.info("Delta is " + delta + "ms");
		Assert.assertTrue(delta < 50);

		// closing
		clientConnection.close();
		serverConnection.close();
		lp.close();
	}

}
