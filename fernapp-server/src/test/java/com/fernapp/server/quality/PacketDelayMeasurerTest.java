package com.fernapp.server.quality;

import junit.framework.Assert;

import org.junit.Test;

import com.fernapp.raup.quality.EchoRequest;
import com.fernapp.raup.quality.EchoResponse;
import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.LoopbackConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.util.Callback;

/**
 * @author Markus
 * 
 */
public class PacketDelayMeasurerTest {

	private MessagingConnection clientConnection;

	@Test
	public void testMeasurement() throws Exception {
		final LoopbackConnection lp = new LoopbackConnection();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientConnection = new JavaSerMessagingConnection(new SocketDataChannel(lp.getClientSocket()),
							"clientMc");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();
		MessagingConnection serverConnection = new JavaSerMessagingConnection(new SocketDataChannel(
				lp.getServerSocket()), "serverMc");
		clientConnectThread.join();
		Assert.assertTrue(serverConnection.isOpen());
		Assert.assertTrue(clientConnection.isOpen());

		try {
			PacketDelayMeasurer measurer = new PacketDelayMeasurer(serverConnection);
			clientConnection.registerReceivedHandler(EchoRequest.class, new Callback<EchoRequest>() {
				public void onCallback(EchoRequest payload) throws Exception {
					clientConnection.sendMessage(new EchoResponse());
				}
			});

			clientConnection.startReceivingMessages();
			serverConnection.startReceivingMessages();

			measurer.measure(10);
			Assert.assertTrue(measurer.getAvgPacketDelay() >= 0);
			Assert.assertTrue(measurer.getAvgPacketDelay() < 100);

		} finally {
			clientConnection.close();
			serverConnection.close();
			lp.close();
		}
	}

}
