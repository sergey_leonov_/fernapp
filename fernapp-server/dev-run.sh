#!/bin/sh

#
# Script to compile and start fernapp server for development purposes.
# Specify app to start as first parameter (otherwise xterm will be started)
#

set -e

# compile everything
(cd ../ && mvn install -DskipTests)

BASE_DIR=`pwd`
RUN_DIR=$BASE_DIR/target/dist
echo Building distribution in $RUN_DIR

# build fernapp distribution
rm -rf $RUN_DIR
mkdir -p $RUN_DIR/lib
cp -v -r $BASE_DIR/dist/* $RUN_DIR/
cp -v -r $BASE_DIR/target/fernapp-server-jar-with-dependencies.jar $RUN_DIR/
cp -v -r $BASE_DIR/target/ext/*.so $RUN_DIR/lib/
cp -v -r $BASE_DIR/target/ext/fernapp-ua-swing-web.zip $RUN_DIR/

(cd $RUN_DIR && ./fernapp-server.sh "$@")
