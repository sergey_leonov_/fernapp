package com.fernapp.uacommon.middleware;

import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.util.Callback;
import com.google.common.base.Stopwatch;

/**
 * @author Markus
 * 
 */
public class MessagingConnectionTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private MessagingConnection clientConnection;
	private transient TestData receivedData;
	private transient Stopwatch stopwatch;

	@Test
	public void testSendReceive() throws Exception {
		final LoopbackConnection lp = new LoopbackConnection();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientConnection = new JsonMessagingConnection(new SocketDataChannel(lp.getClientSocket()),
							"clientMc");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();
		MessagingConnection serverConnection = new JsonMessagingConnection(new SocketDataChannel(lp.getServerSocket()),
				"serverMc");
		clientConnectThread.join();
		Assert.assertTrue(serverConnection.isOpen());
		Assert.assertTrue(clientConnection.isOpen());

		// client receive data
		clientConnection.registerReceivedHandler(TestData.class, new Callback<TestData>() {
			public void onCallback(TestData payload) {
				receivedData = payload;
			}
		});
		clientConnection.startReceivingMessages();
		serverConnection.startReceivingMessages();

		// send data
		TestData d1 = new TestData();
		d1.testString = "hallo";
		int dataSize = 100;
		d1.bigData = new byte[dataSize];
		serverConnection.sendMessage(d1);
		Thread.sleep(200);
		Assert.assertNotNull(receivedData);
		Assert.assertEquals(d1.testString, receivedData.testString);
		Assert.assertNotNull(receivedData.bigData);
		Assert.assertTrue(Arrays.equals(receivedData.bigData, d1.bigData));

		// send same instance with different data
		receivedData = null;
		d1.testString = "change";
		serverConnection.sendMessage(d1);
		Thread.sleep(200);
		Assert.assertNotNull(receivedData);
		Assert.assertEquals(d1.testString, receivedData.testString);

		// closing
		log.info("Received everything. Closing...");
		Assert.assertTrue(serverConnection.isOpen());
		Assert.assertTrue(clientConnection.isOpen());
		clientConnection.close();
		Thread.sleep(500);
		Assert.assertFalse(serverConnection.isOpen());
		Assert.assertFalse(clientConnection.isOpen());

		// closing
		lp.close();
	}

	@Test
	public void performance() throws Exception {
		final LoopbackConnection lp = new LoopbackConnection();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientConnection = new JavaSerMessagingConnection(new SocketDataChannel(lp.getClientSocket()),
							"clientMc");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();
		MessagingConnection serverConnection = new JavaSerMessagingConnection(new SocketDataChannel(
				lp.getServerSocket()), "serverMc");
		clientConnectThread.join();

		// client receive data
		clientConnection.registerReceivedHandler(TestData.class, new Callback<TestData>() {
			public void onCallback(TestData payload) {
				stopwatch.stop();
				receivedData = payload;
			}
		});
		clientConnection.startReceivingMessages();
		serverConnection.startReceivingMessages();

		int repetitions = 100;
		long endToEndDelaySum = 0;
		for (int i = 0; i < repetitions; i++) {
			receivedData = null;
			stopwatch = new Stopwatch();

			// send data
			int dataSize = 50000;
			TestData obj = new TestData();
			obj.bigData = new byte[dataSize];
			stopwatch.start();
			serverConnection.sendMessage(obj);

			// wait
			while (receivedData == null) {
				TimeUnit.MILLISECONDS.sleep(10);
			}

			System.out.println(stopwatch.toString());
			endToEndDelaySum += stopwatch.elapsedTime(TimeUnit.NANOSECONDS);
		}

		long avgEndToEndDelay = endToEndDelaySum / repetitions;
		log.info("Avg. message end-to-end delay: " + avgEndToEndDelay + "ns");

		// closing
		clientConnection.close();
		serverConnection.close();
		lp.close();
	}

	@SuppressWarnings("unused")
	private static class TestData implements Serializable {
		private String testString;
		private byte[] bigData;

		public String getTestString() {
			return testString;
		}

		public void setTestString(String testString) {
			this.testString = testString;
		}

		public byte[] getBigData() {
			return bigData;
		}

		public void setBigData(byte[] bigData) {
			this.bigData = bigData;
		}
	}

}
