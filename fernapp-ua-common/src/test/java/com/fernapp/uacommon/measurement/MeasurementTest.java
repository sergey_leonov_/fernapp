package com.fernapp.uacommon.measurement;

import junit.framework.Assert;

import org.junit.Test;

import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor.MeasurementReport;

/**
 * @author Markus
 * 
 */
public class MeasurementTest {

	@Test
	public void testMeasurements() throws Exception {
		AggregatingMeasurementProcessor measurementProcessor = new AggregatingMeasurementProcessor(10);
		MeasurementReceiver measurementReceiver = new DefaultMeasurementReceiver(measurementProcessor);

		// generate measurements
		for (int i = 0; i < 10; i++) {
			long start = System.currentTimeMillis();
			Thread.sleep(50);
			long delay = System.currentTimeMillis() - start;
			measurementReceiver.receiveMeasurement(DelayCategory.CAPTURE, delay, 0);
		}

		// evaluate
		MeasurementReport report = measurementProcessor.generateReport(DelayCategory.CAPTURE);
		Assert.assertEquals(10, report.getCount());
		Assert.assertTrue(report.getAvgDelay() > 30);
		Assert.assertTrue(report.getAvgDelay() < 70);
	}

}
