package com.fernapp.useragent;

import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.uacommon.middleware.channel.SocketFactory;
import com.fernapp.util.Callback;

/**
 * @author Markus
 * 
 */
public class DefaultConnectionKeeper implements ConnectionKeeper {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private boolean enabled = true;
	private List<Callback<MessagingConnection>> callbacks = new ArrayList<Callback<MessagingConnection>>();
	private MessagingConnection messagingConnection;

	private long connectionRetrySleepMillis = 5000;

	/**
	 * @see com.fernapp.useragent.ConnectionKeeper#keepConnection(com.fernapp.useragent.ApplicationSessionConnectionDetails)
	 */
	public void keepConnection(ApplicationSessionConnectionDetails connectionDetails) {
		SocketAddress socketAddress = new InetSocketAddress(connectionDetails.getHost(), connectionDetails.getPort());

		while (enabled) {
			log.info("Etablishing connection to " + connectionDetails);
			try {
				// create connection
				Socket socket = SocketFactory.createClientSocket();
				socket.connect(socketAddress, 5000);
				messagingConnection = new JavaSerMessagingConnection(new SocketDataChannel(socket), "raup");
				log.info("Connection established");

				// tell everyone that the MessagingConnection has changed
				for (Callback<MessagingConnection> changeCallback : callbacks) {
					changeCallback.onCallback(messagingConnection);
				}

				// register onClose listener
				final CountDownLatch closeLatch = new CountDownLatch(1);
				messagingConnection.setOnCloseListener(new Callback<Void>() {
					public void onCallback(Void payload) {
						closeLatch.countDown();
					}
				});

				// start receiving messages and block until connection is closed
				messagingConnection.startReceivingMessages();
				closeLatch.await();
			} catch (ConnectException e) {
				log.error("Failed to establish connection: " + e.getMessage());
			} catch (Exception e) {
				log.error("Failed to establish connection", e);
			}

			// tell everyone that the MessagingConnection is gone
			messagingConnection = null;
			for (Callback<MessagingConnection> changeCallback : callbacks) {
				try {
					changeCallback.onCallback(messagingConnection);
				} catch (Exception e) {
					log.error("Listener had an error", e);
				}
			}

			try {
				// sleep a bit
				log.info("Waiting " + (connectionRetrySleepMillis / 1000) + " seconds until retry");
				Thread.sleep(connectionRetrySleepMillis);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * @see com.fernapp.useragent.ConnectionKeeper#setEnabled(boolean)
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @see com.fernapp.useragent.ConnectionKeeper#addConnectionChangeListener(com.fernapp.util.Callback)
	 */
	public void addConnectionChangeListener(Callback<MessagingConnection> changeCallback) {
		callbacks.add(changeCallback);
	}

	public MessagingConnection getMessagingConnection() {
		return messagingConnection;
	}

}
