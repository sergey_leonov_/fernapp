package com.fernapp.useragent.decoder;

import java.awt.image.BufferedImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.VideoEncodingType;

/**
 * @author Markus
 * 
 */
public class H264Decoder implements Decoder {

	private static final Logger log = LoggerFactory.getLogger(H264Decoder.class);

	static {
		try {
			System.loadLibrary("avutil-51");
			System.loadLibrary("libx264-115");
			System.loadLibrary("libogg-0");
			System.loadLibrary("libvorbis-0");
			System.loadLibrary("libvorbisenc-2");
			System.loadLibrary("libmp3lame-0");
			System.loadLibrary("avcodec-53");
			System.loadLibrary("swscale-2");
			System.loadLibrary("uadecoder");
		} catch (UnsatisfiedLinkError e) {
			log.error("Failed to load native library (java.library.path: " + System.getProperty("java.library.path")
					+ ")", e);
			throw e;
		}
	}

	private long decCtx;
	private RawDecoder rawDecoder = new RawDecoder();

	public H264Decoder() {
		this.decCtx = h264DecoderInit();
	}

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#getEncodingType()
	 */
	public VideoEncodingType getEncodingType() {
		return VideoEncodingType.H264;
	}

	public BufferedImage decode(int width, int height, byte[] encodedChunk) {
		byte[] rgbFrame = h264DecoderDecode(encodedChunk, width, height, decCtx);
		return rawDecoder.decode(width, height, rgbFrame);
	}

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#shutdown()
	 */
	public void shutdown() {
		h264DecoderShutdown(decCtx);
	}

	private native long h264DecoderInit();

	private native byte[] h264DecoderDecode(byte[] encodedChunk, int width, int height, long decoderContext);

	private native void h264DecoderShutdown(long decoderContext);

}
