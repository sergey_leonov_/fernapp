package com.fernapp.useragent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.Version;
import com.fernapp.raup.VideoEncodingType;
import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.raup.handshake.GreetingResponse;
import com.fernapp.raup.quality.DummyData;
import com.fernapp.raup.quality.EchoRequest;
import com.fernapp.raup.quality.EchoResponse;
import com.fernapp.raup.quality.ReceiveReport;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.DefaultMeasurementReceiver;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.measurement.RemoteMeasurementProcessor;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.ui.RemoteWindowController;
import com.fernapp.util.Callback;

/**
 * Abstract implementation of a {@link RaupClient} that should be useable for swing standalone, swing applet and android
 * user agents.
 * 
 * @author Markus
 */
public abstract class AbstractRaupClient implements RaupClient {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	protected final Map<String, RemoteWindowController> windows = new ConcurrentHashMap<String, RemoteWindowController>();
	private MessagingConnection messagingConnection;
	private MeasurementReceiver measurementReceiver;
	private volatile State state = State.CONNECTING;

	/**
	 * @see com.fernapp.useragent.RaupClient#initForConnection(com.fernapp.uacommon.middleware.MessagingConnection)
	 */
	public void initForConnection(MessagingConnection mc) {
		this.messagingConnection = mc;

		if (messagingConnection != null) {
			state = State.CONNECTED;

			// handle RAUP echo request
			messagingConnection.registerReceivedHandler(EchoRequest.class, new Callback<EchoRequest>() {
				public void onCallback(EchoRequest payload) throws Exception {
					messagingConnection.sendMessage(new EchoResponse());
				}
			});

			// dummy data
			messagingConnection.registerReceivedHandler(DummyData.class, new Callback<DummyData>() {
				public void onCallback(DummyData payload) {
					// discard
				}
			});

			messagingConnection.registerReceivedHandler(WindowSettings.class, new Callback<WindowSettings>() {
				public void onCallback(WindowSettings payload) throws Exception {
					onWindowSettingsChange(payload);
				}
			});

			messagingConnection.registerReceivedHandler(WindowDestroyed.class, new Callback<WindowDestroyed>() {
				public void onCallback(WindowDestroyed payload) throws Exception {
					onWindowDestroyed(payload);
				}
			});

			messagingConnection.registerReceivedHandler(WindowContentUpdate.class, new Callback<WindowContentUpdate>() {
				public void onCallback(WindowContentUpdate payload) throws Exception {
					onWindowContentUpdate(payload);
				}
			});

			messagingConnection.registerReceivedHandler(GreetingResponse.class, new Callback<GreetingResponse>() {
				public void onCallback(GreetingResponse greetingResponse) throws Exception {
					if (!greetingResponse.isAccepted()) {
						showMessage(greetingResponse.getErrorMessage());
						shutdown();
					} else {
						log.info("Handshake successfull. Waiting for window settings...");
					}
				}
			});

			measurementReceiver = new DefaultMeasurementReceiver(new RemoteMeasurementProcessor(messagingConnection));

			sendGreeting();
		} else {
			state = State.RECONNECTING;
			closeAllWindows();
			// TODO gray out frames or close them
		}
	}

	protected abstract void showMessage(String message);

	private void sendGreeting() {
		ClientGreeting greeting = new ClientGreeting();
		greeting.setPassphrase(getPassphrase());
		greeting.setProtocolVersion(Version.RAUP_PROTOCOL_VERSION);
		greeting.setPreferedVideoEncoding(VideoEncodingType.MPNG);
		messagingConnection.sendMessageAsync(greeting);
	}

	protected abstract String getPassphrase();

	/**
	 * @see com.fernapp.useragent.RaupClient#getMessagingConnection()
	 */
	public MessagingConnection getMessagingConnection() {
		return messagingConnection;
	}

	/**
	 * @see com.fernapp.useragent.RaupClient#shutdown()
	 */
	public void shutdown() {
		if (messagingConnection != null) {
			messagingConnection.close();
		}
		closeAllWindows();
	}

	protected void onWindowSettingsChange(WindowSettings windowSettings) {
		if (log.isTraceEnabled()) {
			log.trace("Received window settings " + windowSettings.toString());
		}

		RemoteWindowController remoteWindowController = windows.get(windowSettings.getWindowId());

		// create new local window if necessary
		boolean isNewWindow = (remoteWindowController == null);
		if (isNewWindow) {
			remoteWindowController = createRemoteWindowController(windowSettings);
			windows.put(windowSettings.getWindowId(), remoteWindowController);
		}

		remoteWindowController.onWindowSettingsChange(windowSettings, isNewWindow);
	}

	/**
	 * Creates and returns a {@link RemoteWindowController} that represents the given remote window.
	 */
	protected abstract RemoteWindowController createRemoteWindowController(WindowSettings windowSettings);

	private void onWindowDestroyed(WindowDestroyed windowDestroyed) {
		if (log.isTraceEnabled()) {
			log.trace("Destroying window " + windowDestroyed.getWindowId());
		}

		RemoteWindowController remoteWindowController = windows.remove(windowDestroyed.getWindowId());
		if (remoteWindowController == null) {
			log.warn("onWindowDestroyed could not find the associated window " + windowDestroyed.getWindowId());
		} else {
			remoteWindowController.dispose();
		}
	}

	private void onWindowContentUpdate(WindowContentUpdate windowContentUpdate) throws IOException {
		if (log.isTraceEnabled()) {
			log.trace("Received content update for window " + windowContentUpdate.getWindowId());
		}

		// send receive report
		// TODO send less receive reports
		ReceiveReport receiveReport = new ReceiveReport(windowContentUpdate.getServerSendTime());
		messagingConnection.sendMessageAsync(receiveReport);

		RemoteWindowController remoteWindowController = windows.get(windowContentUpdate.getWindowId());
		if (remoteWindowController == null) {
			log.warn("onWindowContentUpdate could not find the associated window " + windowContentUpdate.getWindowId());
			return;
		}

		remoteWindowController.onWindowContentUpdate(windowContentUpdate);
	}

	private void closeAllWindows() {
		log.info("Closing all windows");
		for (Entry<String, RemoteWindowController> windowEntry : windows.entrySet()) {
			windowEntry.getValue().dispose();
			windows.remove(windowEntry.getKey());
		}
	}

	/**
	 * @see com.fernapp.useragent.RaupClient#getMeasurementReceiver()
	 */
	public MeasurementReceiver getMeasurementReceiver() {
		return measurementReceiver;
	}

	/**
	 * @see com.fernapp.useragent.RaupClient#getParentWindow(com.fernapp.useragent.ui.RemoteWindowController)
	 */
	public RemoteWindowController getParentWindow(RemoteWindowController remoteWindowController) {
		String parentId = remoteWindowController.getWindowSettings().getParentWindowId();
		for (RemoteWindowController rwc : windows.values()) {
			if (rwc.getWindowId().equals(parentId)) {
				return rwc;
			}
		}
		throw new IllegalArgumentException("No parent found for window " + remoteWindowController.getWindowId());
	}

	/**
	 * @see com.fernapp.useragent.RaupClient#getWindows()
	 */
	public List<RemoteWindowController> getWindows() {
		return new ArrayList<RemoteWindowController>(windows.values());
	}

	public State getState() {
		return state;
	}

	public static enum State {
		CONNECTING, CONNECTED, RECONNECTING
	}

}
