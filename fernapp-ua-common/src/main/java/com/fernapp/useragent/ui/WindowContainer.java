package com.fernapp.useragent.ui;

import java.awt.image.BufferedImage;

/**
 * Abstraction of something (JFrame, JWindow, Applet) that can display a remote window. Is controlled by a
 * {@link RemoteWindowController}.
 * <p>
 * The position that the content location methods return is the upper left corner of the content not the frame. Even if
 * the window itself is not yet displayed, a position will be returned as soon as {@link #setContentLocation(int, int)}
 * has been called.
 * <p>
 * The window should not be closeable by the user, but should call the appropriate listener instead.
 * 
 * @author Markus
 */
public interface WindowContainer {

	/**
	 * Set size of content panel.
	 */
	void setContentSize(int width, int height);

	int getContentWidth();

	int getContentHeight();

	/**
	 * Sets the window location by its content panel. The desired coordinates will be remembered even the window is not
	 * yet displayable.
	 */
	void setContentLocation(int left, int top);

	/**
	 * Returns the location (left) of the content panel on screen. Might throw an excpetion if
	 * {@link #setContentLocation(int, int)} has not been called before.
	 */
	int getContentLocationLeft();

	/**
	 * Like {@link #getContentLocationLeft()} for the top coordinate.
	 */
	int getContentLocationTop();

	void setTitle(String title);

	/**
	 * Updates the content that this window container displays.
	 */
	void updateContent(BufferedImage bufferedImage);

	/**
	 * Makes the window visible.
	 */
	void show();

	boolean isVisible();

	void dispose();

	void addInputEventListener(InputEventListener inputEventListener);

	void addWindowStructuralListener(WindowStructuralListener contentResizeListener);

}
