package com.fernapp.raup;

/**
 * List of supported video codecs.
 * 
 * @author Markus
 */
public enum VideoEncodingType {

	RAW(0), H264(1), MPNG(2);

	private int nativeEncodingType;

	private VideoEncodingType(int nativeEncodingType) {
		this.nativeEncodingType = nativeEncodingType;
	}

	public int getNativeEncodingType() {
		return nativeEncodingType;
	}

}
