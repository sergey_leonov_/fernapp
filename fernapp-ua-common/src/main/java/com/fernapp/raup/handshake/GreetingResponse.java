package com.fernapp.raup.handshake;

import java.io.Serializable;

/**
 * The response the server sends to the initial {@link ClientGreeting}. {@link #isAccepted()} tells the client if the
 * server accepts the client in the RAUP session.
 * 
 * @author Markus
 */
public class GreetingResponse implements Serializable {

	private boolean accepted;
	/** If not accepted */
	private String errorMessage;

	public GreetingResponse() {
		// default constructor
	}

	public GreetingResponse(boolean accepted, String errorMessage) {
		this.accepted = accepted;
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the accepted
	 */
	public boolean isAccepted() {
		return accepted;
	}

	/**
	 * @param accepted
	 *            the accepted to set
	 */
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
