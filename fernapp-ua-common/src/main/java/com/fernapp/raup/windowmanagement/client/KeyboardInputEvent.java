package com.fernapp.raup.windowmanagement.client;

import java.awt.event.KeyEvent;

/**
 * A keyboard input event. Uses the symbols defined by Java AWT. Key code, char and location are directly taken from
 * {@link KeyEvent}.
 * 
 * @author Markus
 */
public class KeyboardInputEvent extends InputEvent {

	private int keyCode;
	private char keyChar;
	private int keyLocation;
	/** pressed or released */
	private boolean pressed;

	public KeyboardInputEvent() {
		// default constructor
	}

	public KeyboardInputEvent(String windowId, int keyCode, char keyChar, int keyLocation, boolean pressed) {
		super(windowId);
		this.keyCode = keyCode;
		this.keyChar = keyChar;
		this.keyLocation = keyLocation;
		this.pressed = pressed;
	}

	@Override
	public String toString() {
		String c = (keyChar == KeyEvent.CHAR_UNDEFINED) ? "UNDEFINED" : String.valueOf((int) keyChar);
		return "[windowId=" + getWindowId() + ", keyCode=" + keyCode + ", keyChar=" + keyChar + " (" + c
				+ "), keyLocation=" + keyLocation + ", pressed=" + pressed + "]";
	}

	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	public char getKeyChar() {
		return keyChar;
	}

	public void setKeyChar(char keyChar) {
		this.keyChar = keyChar;
	}

	public int getKeyLocation() {
		return keyLocation;
	}

	public void setKeyLocation(int keyLocation) {
		this.keyLocation = keyLocation;
	}

	public boolean isPressed() {
		return pressed;
	}

	public void setPressed(boolean pressed) {
		this.pressed = pressed;
	}

}
