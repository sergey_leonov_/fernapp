package com.fernapp.raup.windowmanagement.server;

import java.io.Serializable;

import com.fernapp.raup.quality.ReceiveReport;

/**
 * Contains the content update for a window. The client must respond with a {@link ReceiveReport} immediately.
 * 
 * @author Markus
 */
public class WindowContentUpdate implements Serializable {

	private String windowId;
	private VideoStreamChunk videoStreamChunk;
	private long serverSendTime;

	public WindowContentUpdate() {
		// default constructor
	}

	public WindowContentUpdate(String windowId, VideoStreamChunk videoStreamChunk) {
		this.windowId = windowId;
		this.videoStreamChunk = videoStreamChunk;
	}

	public String getWindowId() {
		return windowId;
	}

	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

	public VideoStreamChunk getVideoStreamChunk() {
		return videoStreamChunk;
	}

	public void setVideoStreamChunk(VideoStreamChunk videoStreamChunk) {
		this.videoStreamChunk = videoStreamChunk;
	}

	public long getServerSendTime() {
		return serverSendTime;
	}

	public void setServerSendTime(long serverSendTime) {
		this.serverSendTime = serverSendTime;
	}

}
