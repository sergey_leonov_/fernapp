package com.fernapp.raup.quality;

import java.io.Serializable;

/**
 * Response to a {@link EchoRequest}.
 * 
 * @author Markus
 */
public class EchoResponse implements Serializable {

	// no data

}
