package com.fernapp.raup.quality;

import java.io.Serializable;

/**
 * May be sent by the server to the client. Client must respond immediately with a {@link EchoResponse}.
 * 
 * @author Markus
 */
public class EchoRequest implements Serializable {

	// no payload

}
