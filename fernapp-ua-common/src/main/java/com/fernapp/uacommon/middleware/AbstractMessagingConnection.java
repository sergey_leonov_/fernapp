package com.fernapp.uacommon.middleware;

import java.io.IOException;
import java.security.AccessControlException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.util.Callback;
import com.google.common.base.Preconditions;

/**
 * Abstract implementation of {@link MessagingConnection} utilizing a {@link DataChannel} for byte transport.
 * 
 * @author Markus
 */
public abstract class AbstractMessagingConnection implements MessagingConnection {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private final String name;
	private final Map<Class<?>, Callback<?>> messageHandlers = new HashMap<Class<?>, Callback<?>>();
	private volatile Callback<Void> onCloseCallback;
	private final ExecutorService sendExecutorService = Executors.newFixedThreadPool(1);
	private volatile Long lastReceivedAliveTime;
	private final boolean aliveCheckEnabled = true;

	protected AbstractMessagingConnection(String name) {
		this.name = name;

		// add a listener to handle alive probes from the other side
		// TODO handle all messages as alive probes
		registerReceivedHandler(KeepAlive.class, new Callback<KeepAlive>() {
			public void onCallback(KeepAlive aliveCheckProbe) throws Exception {
				lastReceivedAliveTime = System.currentTimeMillis();
			}
		});
	}

	protected abstract DataChannel getDataChannel();

	/**
	 * Writes the message on the {@link DataChannel}. Must be thread-safe!
	 */
	protected abstract void writeMessage(Object message) throws IOException;

	/**
	 * Blocks and returns a received object. May trigger {@link #close()} if connection is gone. Returns null if there
	 * was a problem.
	 */
	protected abstract Object readMessage();

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#isOpen()
	 */
	public boolean isOpen() {
		return getDataChannel().isOpen();
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#setOnCloseListener(com.fernapp.util.Callback)
	 */
	public void setOnCloseListener(Callback<Void> onCloseCallback) {
		if (this.onCloseCallback != null) {
			log.warn("Overwritting previously registered close callback");
		}
		this.onCloseCallback = onCloseCallback;
		if (!isOpen()) {
			try {
				onCloseCallback.onCallback(null);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#sendMessage(java.lang.Object)
	 */
	public void sendMessage(Object message) throws IOException {
		if (!isOpen()) {
			throw new IOException("Connection is closed");
		}

		if (log.isTraceEnabled()) {
			log.trace("Sending message of type " + message.getClass().getSimpleName());
		}
		try {
			writeMessage(message);
		} catch (IOException e) {
			close();
			throw e;
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#sendMessageAsync(java.lang.Object)
	 */
	public void sendMessageAsync(final Object message) {
		if (isOpen()) {
			sendExecutorService.execute(new Runnable() {
				public void run() {
					try {
						writeMessage(message);
					} catch (Exception e) {
						if (isOpen()) {
							log.error("Message could not be send", e);
						} else {
							log.error("Message could not be send. " + e.getMessage());
						}
					}
				}
			});
		} else {
			log.error("Message cannot be send. Connection is closed.");
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#close()
	 */
	public final void close() {
		boolean weClosedIt = false;

		synchronized (this) {
			if (isOpen()) {
				log.info("Closing connection " + getName());
				try {
					// doesn't work in applets
					sendExecutorService.shutdownNow();
				} catch (AccessControlException e1) {
					log.error("Unable to shutdown thread pool", e1);
				}
				cleanup();
				weClosedIt = true;
			}
			Preconditions.checkState(!isOpen());
		}

		// to prevent deadlock, this is outside the synchronization
		if (weClosedIt && onCloseCallback != null) {
			try {
				onCloseCallback.onCallback(null);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected void cleanup() {
		try {
			getDataChannel().close();
		} catch (IOException e) {
			log.warn("Data channel close had a problem", e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#registerReceivedHandler(java.lang.Class,
	 *      com.fernapp.util.Callback)
	 */
	public synchronized <T> void registerReceivedHandler(Class<T> messageClazz, Callback<T> receiveCallback) {
		messageHandlers.put(messageClazz, receiveCallback);
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#startReceivingMessages()
	 */
	public void startReceivingMessages() {
		if (log.isDebugEnabled()) {
			log.debug("Starting message receive thread of connection " + getName());
		}

		// receive thread
		new Thread(new Runnable() {
			public void run() {
				while (isOpen()) {
					Object obj = readMessage();
					if (obj != null) {
						if (log.isTraceEnabled()) {
							log.trace("Received message of type " + obj.getClass().getSimpleName());
						}
						deliverMessage(obj);
					}
				}
				if (log.isDebugEnabled()) {
					log.debug("Receive thread of connection " + getName() + " stopped");
				}
			}
		}, getName() + "Receive").start();

		if (aliveCheckEnabled) {
			// start alive check
			lastReceivedAliveTime = System.currentTimeMillis();
			if (log.isDebugEnabled()) {
				log.debug("Starting alive check thread of connection " + getName());
			}
			Thread aliveChecker = new Thread(new Runnable() {
				public void run() {
					aliveCheck();
					if (log.isDebugEnabled()) {
						log.debug("Alive check thread of connection " + getName() + " stopped");
					}
				}
			}, getName() + "AliveCheck");
			aliveChecker.start();
		}
	}

	protected <T> void deliverMessage(T message) {
		Class<?> messageClazz = message.getClass();

		while (messageClazz != null) {
			Callback<T> handler = (Callback<T>) messageHandlers.get(messageClazz);
			if (handler != null) {
				try {
					handler.onCallback(message);
				} catch (Exception e) {
					log.error("Message handler has an error", e);
				}
				return;
			}
			messageClazz = messageClazz.getSuperclass();
		}
		log.error("No handler for " + message.toString());
	}

	/**
	 * Check if the other side is still there.
	 */
	private void aliveCheck() {
		long aliveCheckDelay = MessagingConnection.CONNECTION_TIMEOUT / 5;
		while (isOpen()) {
			try {
				TimeUnit.MILLISECONDS.sleep(aliveCheckDelay);
			} catch (InterruptedException e) {
				log.error("Sleep interupted", e);
			}

			// tell the other side that we are alive
			sendMessageAsync(new KeepAlive());

			// check that the other side has sent alive probes
			long lastAliveProbeDelay = System.currentTimeMillis() - lastReceivedAliveTime;
			if (lastAliveProbeDelay > MessagingConnection.CONNECTION_TIMEOUT) {
				log.info("Connection has timed out (other side has stopped sending alive probes)");
				close();
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see com.fernapp.uacommon.middleware.MessagingConnection#getRemoteEndpointDescriptor()
	 */
	public String getRemoteEndpointDescriptor() {
		return getDataChannel().getRemoteEndpointDescriptor();
	}

	@Override
	public String toString() {
		return "connection name: " + getName() + ", endpoint: " + getRemoteEndpointDescriptor();
	}

}
