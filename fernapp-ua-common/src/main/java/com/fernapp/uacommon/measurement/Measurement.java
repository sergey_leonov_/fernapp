package com.fernapp.uacommon.measurement;

import java.io.Serializable;

/**
 * A single measurement for certain {@link DelayCategory}.
 * 
 * @author Markus
 */
public class Measurement implements Serializable {

	private long timestamp;
	private DelayCategory delayCategory;
	private long delay;
	private long dataSize;

	public Measurement() {
		// default constructor
	}

	public Measurement(long timestamp, DelayCategory delayCategory, long delay, long dataSize) {
		this.timestamp = timestamp;
		this.delayCategory = delayCategory;
		this.delay = delay;
		this.dataSize = dataSize;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public DelayCategory getDelayCategory() {
		return delayCategory;
	}

	public void setDelayCategory(DelayCategory delayCategory) {
		this.delayCategory = delayCategory;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public long getDataSize() {
		return dataSize;
	}

	public void setDataSize(long dataSize) {
		this.dataSize = dataSize;
	}

}
