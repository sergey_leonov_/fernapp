#include <pthread.h>
#include <unistd.h>
#include "util/common.h"
#include "util/windowDataManager.h"


void testWindowDataArray() {
	lockWindowData();

	WindowId windowId = 10;
	WindowId innerWindowId = 11;

	WindowData *wd = addWindowData(windowId);
	wd->innerWindowId = innerWindowId;

	getWindowData(windowId,0,0)->pixmap=13;
	assert( getInnerWindowId(10) == 11 );

	wd = removeWindowData(windowId);
	free(wd);
	assert( getInnerWindowId(10) == 0 );

	unlockWindowData();
}


void *concurrentTask(void *arg) {
	lockWindowData();
	assert( getWindowData(1122,0,0)==NULL );
	unlockWindowData();
	return NULL;
}


void testConcurrency() {
	lockWindowData();
	addWindowData(1122);

	pthread_t task;
	int arg = 0;
	pthread_create(&task, NULL, concurrentTask, &arg);
	// microsecond sleep
	usleep(100 * 1000);
	removeWindowData(1122);
	unlockWindowData();

	pthread_join(task,NULL);
}


int main() {
	_log = printf_log;

	initWindowDataManagement();
	testWindowDataArray();
	testConcurrency();
	shutdownWindowDataManagement();

	return 0;
}
