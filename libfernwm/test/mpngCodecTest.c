#include "util/common.h"
#include "encoders/mpngEncoder.h"
#include "encoders/encoderUtil.h"


void testEncodeAndDecode(char *inputImage) {
	// prepare encoder
	void *encCtx = mpngEncoderInit();

	// read image
	WindowContent *wc = readWindow(inputImage);
	uint8_t *orgRgbData = convertBGRAtoRGB(wc->width, wc->height, wc->imageDate, wc->dataSize);
	writePPM("mpngorgImage.ppm", wc->width, wc->height, (PPMPixel*) orgRgbData);
	free(orgRgbData);

	// encode
	DamageReport damageReport = {0, 0, wc->width, wc->height};
	int dataSize;
	Bool reset;
	uint8_t *encodedChunk = mpngEncoderEncode(wc, damageReport, &dataSize, &reset, encCtx);

	if( encodedChunk != NULL ) {
		logInfo("Encoding successful");

		// prepare decoder
		/*
		// decode
		void *decCtx = mpngDecoderInit();
		int decodedSize;
		uint8_t *rgbImage = mpngDecoderDecode( encodedChunk, dataSize, wc->width, wc->height, &decodedSize, decCtx );
		assert((wc->width) * wc->height *3 == decodedSize);

		// write RGB data
		writePPM("bin/mpngdecoded.ppm", wc->width, wc->height, (PPMPixel*) rgbImage);
		free(rgbImage);

		logInfo("Decoding successful");
		mpngDecoderShutdown(decCtx);
		*/

		free(encodedChunk);
	}

	mpngEncoderShutdown(encCtx);
	freeWindowContent(wc);
}


int main() {
	_log = printf_log;
	testEncodeAndDecode("../test/codecTest_window.bgra");
	testEncodeAndDecode("../test/codecTest_notdiv2.bgra");
	testEncodeAndDecode("../test/codecTest_tooltip.bgra");
	return 0;
}
