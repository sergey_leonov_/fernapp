#include "encoderUtil.h"
#include "rawEncoder.h"


typedef struct RawEncoderContext_type {
} RawEncoderContext;


void *rawEncoderInit() {
	RawEncoderContext *ctx = (RawEncoderContext*) malloc(sizeof(RawEncoderContext));
	return ctx;
}


uint8_t *rawEncoderEncode(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *encoderContext) {
	// convert to RGB
	// writeImage(wc->imageDate, wc->dataSize, wc->width, wc->height, "window_notdiv2.bgra");
	ASSERT(wc->pixelFormat == BGRA_PIXEL_FORMAT);
	uint8_t *chunkData = convertBGRAtoRGB(wc->width, wc->height, wc->imageDate, wc->dataSize);
	*dataSize = wc->width * wc->height * 3;
	*reset = False;
	return chunkData;
}


void rawEncoderShutdown(void *encoderContext) {
	RawEncoderContext *ctx = (RawEncoderContext*) encoderContext;
	free(ctx);
}
