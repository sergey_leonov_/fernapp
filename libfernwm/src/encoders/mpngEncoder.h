#ifndef MPNGENCODER_H_
#define MPNGENCODER_H_

#include "../util/common.h"


void *mpngEncoderInit();
uint8_t *mpngEncoderEncode(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *ctx);
void mpngEncoderShutdown(void *ctx);

#endif /* MPNGENCODER_H_ */
