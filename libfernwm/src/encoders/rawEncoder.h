#ifndef RAWENCODER_H_
#define RAWENCODER_H_

#include "../util/common.h"


void *rawEncoderInit();
uint8_t *rawEncoderEncode(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *ctx);
void rawEncoderShutdown(void *ctx);

#endif /* RAWENCODER_H_ */
