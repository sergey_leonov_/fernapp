#include "windowDataManager.h"


#define maxWindowsCount 20
WindowData windowDataArray[maxWindowsCount];

// TODO evaluate concurrency performance. maybe a GStaticRWLock is better
GStaticRecMutex windowDataRecMutex;


void initWindowDataManagement() {
	IGNORE_DEPRECATED( g_thread_init(NULL) );
	memset(windowDataArray, 0, sizeof(WindowData)*maxWindowsCount);
	IGNORE_DEPRECATED( g_static_rec_mutex_init(&windowDataRecMutex) );
}

void shutdownWindowDataManagement() {
	IGNORE_DEPRECATED( g_static_rec_mutex_free(&windowDataRecMutex) );
}


void lockWindowData() {
	IGNORE_DEPRECATED( g_static_rec_mutex_lock(&windowDataRecMutex) );
}


void unlockWindowData() {
	IGNORE_DEPRECATED( g_static_rec_mutex_unlock(&windowDataRecMutex) );
}


void initializeWindowData(WindowData *wd) {
	wd->windowId = 0;
	wd->innerWindowId = 0;
	wd->pixmap = 0;
	wd->width = 0;
	wd->height = 0;
	wd->title = NULL;
}


WindowData *addWindowData(WindowId windowId) {
	ASSERT(windowId!=0);
	lockWindowData();

	WindowData *windowData = NULL;
	for(int i=0; i<maxWindowsCount; i++) {
		WindowData *wd = windowDataArray + i;
		if(wd->windowId==0) {
			// found a free slot
			initializeWindowData(wd);
			wd->windowId = windowId;
			windowData = wd;
			break;
		}
	}
	if(windowData==NULL) {
		logError("Too many open windows");
	}

	unlockWindowData();
	return windowData;
}


WindowData *removeWindowData(WindowId windowId) {
	ASSERT(windowId!=0);
	WindowData *wdCopy = NULL;

	lockWindowData(); {
		WindowData *wd = getWindowData(windowId,0,0);
		if(wd!=NULL) {
			wdCopy = malloc(sizeof(WindowData));
			memcpy_wrapper(wdCopy, wd, sizeof(WindowData));
			initializeWindowData(wd);
		} else {
			fatal("Missing window data %lu", windowId);
		}
	}
	unlockWindowData();
	return wdCopy;
}


WindowData *getWindowData(WindowId windowId, WindowId innerWindowId, unsigned long pixmap) {
	ASSERT(windowId!=0 || innerWindowId!=0 || pixmap!=0 );
	// mutex must be locked by the caller
	WindowData *windowData = NULL;
	for(int i=0; i<maxWindowsCount; i++) {
		WindowData *wd = windowDataArray + i;
		// the arguments can either be 0 or must be equal
		if( (windowId==0 || windowId == wd->windowId) && (innerWindowId==0 || innerWindowId == wd->innerWindowId) && (pixmap==0 || pixmap == wd->pixmap)) {
			windowData = wd;
		}
	}
	return windowData;
}


WindowId getInnerWindowId(WindowId windowId) {
	WindowId innerWindowId = 0;
	lockWindowData(); {
		WindowData *wd = getWindowData(windowId,0,0);
		if(wd!=NULL) {
			innerWindowId = wd->innerWindowId;
		}
	}
	unlockWindowData();
	return innerWindowId;
}


WindowId getWindowIdByPixmap(unsigned long pixmap) {
	WindowId windowId = 0;
	lockWindowData(); {
		WindowData *wd = getWindowData(0,0,pixmap);
		if(wd!=NULL) {
			windowId = wd->windowId;
		}
	}
	unlockWindowData();
	return windowId;
}


Bool isWindowKnown(WindowId windowId) {
	Bool known;
	lockWindowData(); {
		WindowData *wd = getWindowData(windowId, 0, 0);
		known = (wd!=NULL);
	}
	unlockWindowData();
	return known;
}


jobject buildWindowSettingsObject(JNIEnv *env, WindowData *wd) {
	jstring windowIdString = buildWindowIdString(env, wd->windowId);
    jstring titleStr = (*env)->NewStringUTF(env, wd->title);
	jstring parentWindowIdString = NULL;
	if(wd->parentWindowId != 0) {
		parentWindowIdString = buildWindowIdString(env, wd->parentWindowId);
	}

	jobject windowSettings = (*env)->NewObject(env, class_WindowSettings, method_windowSettings_ctor,
				windowIdString, titleStr, (jint)wd->width, (jint)wd->height, parentWindowIdString, (jint)wd->positionX, (jint)wd->positionY );

	(*env)->DeleteLocalRef(env, windowIdString);
	(*env)->DeleteLocalRef(env, titleStr);
	if(wd->parentWindowId != 0) {
		(*env)->DeleteLocalRef(env, parentWindowIdString);
	}

	handlePotentialJniErrors(env);
	return windowSettings;
}


jobject getOpenWindows(JNIEnv *env) {
	jobject object_ArrayList = (*env)->NewObject(env, class_ArrayList, method_ArrayList_ctor);

	lockWindowData(); {
		for(int i=0; i<maxWindowsCount; i++) {
			WindowData *wd = windowDataArray + i;
			if( wd->windowId!=0 ) {
				// convert and add to arraylist
				jobject windowSettings = buildWindowSettingsObject(env, wd);
				(*env)->CallBooleanMethod(env, object_ArrayList, method_ArrayList_add, windowSettings);
				(*env)->DeleteLocalRef(env, windowSettings);
			}
		}

	}
	unlockWindowData();

	handlePotentialJniErrors(env);
	return object_ArrayList;
}
