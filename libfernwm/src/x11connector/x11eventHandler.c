#include <X11/Xlib.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xrender.h>
#include <X11/extensions/Xdamage.h>
#include <X11/extensions/Xfixes.h>
#include <util/windowDataManager.h>
#include <x11connector/x11util.h>
#include <x11connector/x11connector.h>
#include <x11connector/x11eventHandler.h>
#include <x11connector/x11callback.h>
#include <x11connector/x11capture.h>


/**
 * Identifies to which parent a window might belong to and its relative position to it.
 * Returns true on success.
 */
Bool identifyBelongsToWindow(Display *dpy, WindowId windowId, WindowId innerWindowId, int innerPosX, int innerPosY,
		WindowId *parentId, int *parentPosX, int *parentPosY) {

	// WM_TRANSIENT_FOR specifies the window it belongs to.
	// may not work for pointer grabbing windows like menus and tooltips, but should work for popups
	/*
	WindowId transientForWindow = 0;
	X_CATCH_N_LOG( WindowId transientForInnerWindow = getPropertyValueAsLu(dpy, innerWindowId, WM_TRANSIENT_FOR, False) );
	if(transientForInnerWindow != 0) {
		lockWindowData();
		WindowData *windowData = getWindowData(0,transientForInnerWindow,0);
		if(windowData != NULL) {
			transientForWindow = windowData->windowId;
		}
		unlockWindowData();
		logDebug("WM_TRANSIENT_FOR: belongs to %lu", transientForWindow);
		logInfo("Probably modal window!!!");
	}

	// is it modal?
	if( transientForWindow ) {
		X_CATCH_N_LOG( Atom wmState = getPropertyValueAsLu(dpy, transientForInnerWindow, _NET_WM_STATE, False) );
		if( wmState & _NET_WM_STATE_MODAL ) {
			logInfo("A modal window!!!");
		}
	}
	WindowId belongsToWindow = transientForWindow;
	*/

	WindowId belongsToWindow = 0;

	if( !belongsToWindow ) {
		// main window might also be identified using the stacking order
		Window root_win, parent_win;
		unsigned int num_children;
		Window *child_list;
		X_CATCH_N_LOG( XQueryTree(dpy, root, &root_win, &parent_win, &child_list, &num_children) );
		// child_list is order by the stacking order. the most backward window comes first
		// let's find the top-most main window
		for( int i=num_children-1; i>=0; i-- ){
			Window childId = child_list[i];
			Window childInnerWindowId = getInnerWindowId(childId);
			if( childInnerWindowId != 0 ) {
				belongsToWindow = childId;
				break;
			}
		}
		if( child_list != NULL ) {
			XFree(child_list);
		}

		logTrace("WM_TRANSIENT_FOR not set. Using top-most window %lu", *parentId);

		// alternatives:
		// _NET_ACTIVE_WINDOW: which window has the focus
		// _NET_WM_USER_TIME: last user activity timestamp
	}

	if(belongsToWindow != 0) {
		// and relative position of popup to parent
		// TODO should be changed to relative position to inner window
		// (note: XGetWindowAttributes cannot be used on a inner window to get its on screen location)
		XWindowAttributes parentAttr;
		X_CATCH_N_LOG( XGetWindowAttributes(dpy, belongsToWindow, &parentAttr) );
		*parentPosX = innerPosX - parentAttr.x;
		*parentPosY = innerPosY - parentAttr.y;

		*parentId = belongsToWindow;
		return True;
	} else {
		return False;
	}
}


/**
 * Handles a window map event.
 */
void handleWindowMap(Display *dpy, WindowId windowId, WindowId parentId, Bool override_redirect) {
	logTrace("MapNotify received");

	ASSERT(windowId!=0); // we define that a windowId==0 indicates an error, hopefully it's never used by the X server
	ASSERT(parentId==root); // not strictly necessary, but this the observed X server behaviour

	// determine inner window and it's type
	X_FAIL_FAST( WindowId innerWindowId = identifyInnerWindow(dpy, windowId) );
	ASSERT(innerWindowId != 0);

	XWindowAttributes innerWindowAttributes;
	X_FAIL_FAST( XGetWindowAttributes(dpy, innerWindowId, &innerWindowAttributes) );

	// is it a main window or a popup etc? determine type of window
	X_FAIL_FAST( Atom windowType = getPropertyValueAsLu(dpy, innerWindowId, _NET_WM_WINDOW_TYPE, False) );
	char windowTypeString[100] = "UNKOWN";
	if( windowType != 0 ) {
		char *wtsBuffer = XGetAtomName(dpy, windowType);
		strcpy(windowTypeString, wtsBuffer);
		XFree(wtsBuffer);
	}

	logDebug("Window mapped [windowId=%lu, innerWindowId=%lu, type=%s, override_redirect=%i]", windowId, innerWindowId, windowTypeString, override_redirect);

	// when opening a context menu a hack window appears (type=0, width=10, height=10). we need to filter it out!
	if(windowType == 0 && innerWindowAttributes.width<=10 && innerWindowAttributes.height<=10) {
		logDebug("Window %lu ignored because it is a context menu hack window", windowId);
		return;
	}

	Bool isMainWindow;
	// override_redirect==0 is the main indicator that it is a main window
	if(override_redirect) {
		isMainWindow = False;
	} else {
		isMainWindow = True;
		// old considerations:
		// if windowId != innerWindowId it is a safe sign that it is a main window, but would only work if a WM is used
		// if windowType == 0 we treat it as a main window (AWT does not set the window type)
		// _NET_WM_WINDOW_TYPE_UTILITY are menus, tooltips and toolboxes in OpenOffice
		// isMainWindow = (windowType == 0 || windowType == _NET_WM_WINDOW_TYPE_NORMAL || windowType == _NET_WM_WINDOW_TYPE_DIALOG || windowType == _NET_WM_WINDOW_TYPE_SPLASH);
	}

	// if it is not a main window, we need to determine to which window it belongs to
	Window parentWindowId = 0;
	int positionX = 0;
	int positionY = 0;
	if( !isMainWindow ) {
		// should not have been decorated by WM
		ASSERT(windowId == innerWindowId);
		if( !identifyBelongsToWindow(dpy, windowId, innerWindowId, innerWindowAttributes.x, innerWindowAttributes.y, &parentWindowId, &positionX, &positionY) ) {
			logWarn("Failed to determine parent of child window. Treating it as a main window.");
		}
	}

	// register for property changes
	X_FAIL_FAST( XSelectInput(dpy, innerWindowId, PropertyChangeMask) );
	//X_FAIL_FAST( XSelectInput(dpy, innerWindowId, PointerMotionMask | ButtonPressMask | ButtonReleaseMask) );

	// get title
	char *windowTitle;
	X_FAIL_FAST( Status fetchNameSuccess = XFetchName(dpy, innerWindowId, &windowTitle) );
	if(!fetchNameSuccess) {
		logWarn("No title found for window %lu", innerWindowId);
		windowTitle = malloc(8);
		strcpy(windowTitle, "");
	}

	// until now: if the window was quickly destroyed, nothing would get inconsistent and we could use X_FAIL_FAST.

	// save window data and deliver event in one atomic operation
	lockWindowData(); {
		WindowData *wd = addWindowData(windowId);
		wd->innerWindowId = innerWindowId;
		wd->pixmap = 0;
		wd->width = innerWindowAttributes.width;
		wd->height = innerWindowAttributes.height;
		wd->title = windowTitle;
		wd->parentWindowId = parentWindowId;
		wd->positionX = positionX;
		wd->positionY = positionY;
		// inform executor
		onWindowSettingsChange(wd, True);
		// setup pixmap. must happen in the same atomic operation (handler onWindowSettingsChange will not be able to read the pixmap before we're done here)
		setupCapturing(dpy, windowId);
	}
	unlockWindowData();

}


void handleWindowUnmap(Display *dpy, WindowId windowId, WindowId parentId) {
	// somehow UnmapNotify might be triggered twice for the same window

	lockWindowData();

	if( isWindowKnown(windowId) ) {
		WindowData *windowData = removeWindowData(windowId);
		logDebug("Window unmapped [windowId=%lu]", windowData->windowId);
		free(windowData->title);
		if( windowData->pixmap != 0 ) {
			X_CATCH_N_LOG( XFreePixmap(dpy, windowData->pixmap) );
		}
		onDestroy(windowData->windowId);
		free(windowData);
	} else {
		logTrace("FernWM received a UnmapNotify for unknown window %lu", windowId);
	}

	unlockWindowData();
}


void handlePropertyChange(Display *dpy, WindowId innerWindowId, Atom propertyAtom) {
	logTrace("Handling property change (innerWindowId=%lu)", innerWindowId);

	// title
	if( propertyAtom == WM_NAME ) {
		char *windowTitle;
		X_CATCH_N_LOG( Status fetchNameSuccess = XFetchName(dpy, innerWindowId, &windowTitle) );
		if(fetchNameSuccess) {
			WindowId windowId = 0;

			lockWindowData(); {
				WindowData *wd = getWindowData(0, innerWindowId, 0);
				if(wd != NULL) {
					windowId = wd->windowId;
					free(wd->title);
					wd->title = windowTitle;
					onWindowSettingsChange(wd, False);
				} else {
					logWarn("Missing window (innerWindowId=%lu). Has probably been unmapped inbetween", innerWindowId);
				}
			}
			unlockWindowData();

			if( windowId != 0 ) {
				logDebug("Title of window %lu changed to '%s'", windowId, windowTitle);
			} else {
				logWarn("Window title has changed but window is now gone");
			}
		} else {
			logWarn("handlePropertyChange: No title found for window %lu", innerWindowId);
		}
	}

	//char *atomName = XGetAtomName(dpy, propertyAtom);
	//logDebug("property %lu (%s) on window %lu has changed", propertyAtom, atomName, windowId);
	//XFree(atomName);
}


/**
 * Size change event. Will be triggered for any windows, not just for our windows.
 */
void handleReconfiguration(Display *dpy, WindowId windowId, int width, int height) {
	logTrace("Handling reconfiguration notification (window=%lu, width=%i, height=%i)", windowId, width, height);

	if( isWindowKnown(windowId) ) {
		// TODO check if size has actually changed
		setupCapturing(dpy, windowId);
	} else {
		// happens when a window is first created and reconfigured by our #handleMapRequest code
	}
}


void handleDamage(Display *dpy, XDamageNotifyEvent *damageEvent) {
	ASSERT(damageEvent->drawable != 0);
	WindowId windowId = getWindowIdByPixmap(damageEvent->drawable);
	if(windowId != 0) {
		//logDebug("damage in x=%hi, y=%hi, width=%hu, height=%hu", damageEvent->area.x, damageEvent->area.y, damageEvent->area.width, damageEvent->area.height);
		ASSERT(damageEvent->area.x >= 0);
		if( damageEvent->area.width > 0 && damageEvent->area.height ) {
			onWindowContentUpdate(windowId, damageEvent->area.x, damageEvent->area.y,  damageEvent->area.width, damageEvent->area.height);
		} else {
			logWarn("Empty damage");
		}
	} else {
		// possibly no clients are currently connected
		logWarn("Cannot handle damage for drawable %lu", damageEvent->drawable);
	}
}


/**
 * Only we can make a change to a window's size and position due to SubstructureRedirectMask.
 * We'll intercept our own resize request from x11input#requestWindowResize and application resize requests.
 */
void handleConfigureRequest(Display *dpy, XConfigureRequestEvent *request) {
	WindowId windowId = request->window;
	logTrace("Handling window configure request (window=%lu)", windowId);

	XWindowChanges xwc;
	unsigned long xwcm = request->value_mask;

	// limit width and height to screen (otherwise it won't be reachable by mouse)
	xwc.width = MIN(request->width, screenWidth);
	xwc.height = MIN(request->height, screenHeight);
	xwc.x = 0;
	xwc.y = 0;
	xwc.border_width = request->border_width;
	xwc.stack_mode = request->detail;
	xwc.sibling = request->above;

	X_CATCH_N_LOG( Status status = XConfigureWindow(dpy, windowId, xwcm, &xwc) );
	if(status != 1) {
		logWarn("Failed to configure window (window=%lu)", windowId);
	}
}


/**
 * Allows us to do real window management and control the position and dimension of windows.
 */
void handleMapRequest(Display *dpy, WindowId windowId) {
	logTrace("Handling a window map request (window=%lu)", windowId);

	// apparently handleConfigureRequest will always be called when mapping a new window, so we don't need to do anything here

	/*
	// set position
	X_CATCH_N_LOG( XMoveWindow(dpy, windowId, 0, 0) );

	// limit size if necessary
	// retrieve the dimension of the window
	Window rootWindow;
	int x_return, y_return;
	unsigned int width_return, height_return, border_width_return, depth_return;
	Status ggSuccess = 0;
	X_CATCH_N_LOG( ggSuccess = XGetGeometry(dpy, windowId, &rootWindow, &x_return, & y_return, &width_return, &height_return, &border_width_return, &depth_return) );
	if(ggSuccess) {
		handleResizeRequest(dpy, windowId, width_return, height_return);
	}
	*/

	X_CATCH_N_LOG( XMapWindow(dpy, windowId) );
}


void x11EventLoop() {
	Display *dpy = mainDpy;
	const int xDamageEventType = XDamageNotify + xDamageEventBase;

	// create a dummy window, that we can use to end the blocking XNextEvent call
    X_CATCH_N_LOG( interClientCommunicationWindow = XCreateSimpleWindow(dpy, root, 10, 10, 10, 10, 0, 0, 0) );
    X_CATCH_N_LOG( XSelectInput(dpy, interClientCommunicationWindow, StructureNotifyMask) );

	// event handling
	XEvent event;
	while(x11ConnectorActive) {
		logTrace("Waiting for next event");
		XNextEvent(dpy, &event);


		if( event.type == CreateNotify ) {
			logTrace("CreateNotify received. Nothing to do.");

		} else if( event.type == MapRequest ) {
			handleMapRequest(dpy, event.xmaprequest.window);

		} else if( event.type == ConfigureRequest ) {
			XConfigureRequestEvent *request = &(event.xconfigurerequest);
			handleConfigureRequest(dpy, request);

		} else if( event.type == MapNotify ) {
			handleWindowMap(dpy, event.xmap.window, event.xmap.event, event.xmap.override_redirect);

		} else if( event.type == UnmapNotify ) {
			handleWindowUnmap(dpy, event.xunmap.window, event.xunmap.event);

		} else if( event.type == DestroyNotify ) {
			logTrace("DestroyNotify received. Nothing to do.");

		} else if( event.type == PropertyNotify ) {
			handlePropertyChange(dpy, event.xproperty.window, event.xproperty.atom);

		} else if( event.type == ConfigureNotify ) {
			handleReconfiguration(dpy, event.xconfigure.window, event.xconfigure.width, event.xconfigure.height);

		} else if( event.type == xDamageEventType ) {
			logTrace("DamageEvent received");
			// take application processing time measurement
			// FIXME there is a wrong-measured delay (included in the application processing time), because damage events are processed in a queue with many other events
			if(applicationProcessingStart != 0){
				unsigned long delay = getCurrentTimeMillis() - applicationProcessingStart;
				applicationProcessingStart = 0;
				reportMeasurement(3, delay, 0);
			}

			XDamageNotifyEvent *dev = (XDamageNotifyEvent*)&event;
			// we use this event to determine the server time
			informAboutServerTime(dev->timestamp);
			handleDamage(dpy, dev);

			// allows damage client to clear out all of the damage region
			// it also be an option to use XDamageReportRawRectangles then we don't have to do XDamageSubtract
			X_CATCH( XDamageSubtract(dpy, dev->damage, None, None) );
			if(x11ErrorMessage) {
				if( x11ErrorCode == xDamageErrorBase + BadDamage ) {
					// might cause an BadDamage error if window has already been destroyed
				} else {
					logError("XDamageSubtract reported an error: %s", x11ErrorMessage);
				}
			}

		} else if( event.type == ClientMessage ) {
			logTrace("ClientMessage received. Nothing to do.");

		} else {
			logWarn("Unhandled X11 event type %i", event.type);
		}
		/*
		else if( event.type == MotionNotify ) {
			logDebug("motion: window=%lu, subwindow=%lu, x=%i, y=%i, send_event=%i", event.xmotion.window, event.xmotion.subwindow, event.xmotion.x, event.xmotion.y, event.xmotion.send_event);
		} else if( event.type == ButtonPress || event.type == ButtonRelease ) {
			logDebug("buttonPress: window=%lu, subwindow=%lu, x=%i, y=%i, button=%iu",
					event.xbutton.window, event.xbutton.subwindow, event.xbutton.x, event.xbutton.y, event.xbutton.button);
		}
		*/
	}

	// clean up
	// no further operations can then be executed - this will be guaranteed by the higher level java class X11ApplicationExecutor
	// we use this to protect against a race condition with the clean up code
	lockWindowData();
	unlockWindowData();
	logInfo("X11 event processing has ended - cleaning up");
	XCloseDisplay(mainDpy); // performs XSync internally
	shutdownWindowDataManagement();
	shutdownX11Util();
}
