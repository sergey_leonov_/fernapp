#ifndef X11CONNECTOR_H_
#define X11CONNECTOR_H_


#include <X11/Xlib.h>
#include <util/common.h>

Bool x11ConnectorActive;
Display *mainDpy;
WindowId root;
int screenWidth;
int screenHeight;
int xDamageEventBase;
int xDamageErrorBase;

Atom _NET_WM_WINDOW_TYPE;
Atom _NET_WM_WINDOW_TYPE_NORMAL;
Atom _NET_WM_WINDOW_TYPE_SPLASH;
Atom _NET_WM_WINDOW_TYPE_DIALOG;
Atom _NET_WM_WINDOW_TYPE_UTILITY;
Atom _NET_WM_STATE;
Atom _NET_WM_STATE_MODAL;
Atom WM_NAME;
Atom WM_TRANSIENT_FOR;
Atom WM_PROTOCOLS;
Atom WM_DELETE_WINDOW;

Window interClientCommunicationWindow;


/** hack to measure the application processing time for click actions */
unsigned long applicationProcessingStart;

/** Initializes the connector. Returns if that was successful. */
Bool x11ConnectorInit(const char *displayPort);

void x11Shutdown();


#endif /* X11CONNECTOR_H_ */
