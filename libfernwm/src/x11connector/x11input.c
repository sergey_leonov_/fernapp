#include <X11/keysymdef.h>
#include <X11/extensions/XTest.h>
#include "../util/windowDataManager.h"
#include "../encoders/encoderUtil.h"
#include "x11util.h"
#include "x11connector.h"
#include "x11input.h"
#include "x11capture.h"


const int LEFT_BUTTON = 1;
const int RIGHT_BUTTON = 3;

/** state of control key and pointer buttons */
unsigned int inputState;

WindowId lastActiveWindow = 0;


void movePointer(WindowId windowId, int x, int y) {
	WindowId innerWindowId = getInnerWindowId(windowId);
	if( innerWindowId ) {
		logTrace("Requesting to move pointer (windowId=%lu, innerWindowId=%lu, x=%i, y=%i)",
				windowId, innerWindowId, x, y);

		Display *dpy = getDisplay();

		if(windowId != lastActiveWindow) {
			X_CATCH_N_LOG( XRaiseWindow(dpy, windowId) );
			lastActiveWindow = windowId;
		}

		// catch and ignore errors (usually the window has been unmapped in-between)
		X_CATCH_N_LOG( XWarpPointer(dpy, None, innerWindowId, 0, 0, 0, 0, x, y) );

		returnDisplay(dpy);
	}
}


void pointerButtonAction(WindowId windowId, int x, int y, Bool buttonPressed, int buttonNumber) {
	WindowId innerWindowId = getInnerWindowId(windowId);
	if( innerWindowId ) {
		logTrace("Requesting a pointer button action (windowId=%lu, innerWindowId=%lu, x=%i, y=%i, buttonPressed=%i, buttonNumber=%i)",
				windowId, innerWindowId, x, y, buttonPressed, buttonNumber);

		Display *dpy = getDisplay();

		// which button
		unsigned int button;
		if( buttonNumber == LEFT_BUTTON ) {
			button = Button1;
		} else if( buttonNumber == RIGHT_BUTTON ) {
			button = Button3;
		} else {
			logError("Unknown button number %i", buttonNumber);
			return;
		}

		// determine the application processing time for click actions
		// delay is the time from releasing the button to receiving the damage notification
		if(!buttonPressed && applicationProcessingStart == 0){
			applicationProcessingStart = getCurrentTimeMillis();
		}

		X_CATCH_N_LOG( XTestFakeButtonEvent(dpy, button, buttonPressed, 0) );

		returnDisplay(dpy);


		/**
		 * work mostly. but not for menus in openoffice.
		 * could be due to the fact that the button state is not correctly sent to openoffice
		 * during the EnterNotify event that occurs after a click on a menu.

		Display *dpy = getDisplay();

		XEvent event;
		fillInputEvent(dpy, &event, windowId, innerWindowId, x, y);

		// press or release
		long eventMask;
		if( buttonPressed ) {
			event.type = ButtonPress;
			eventMask = ButtonPressMask;
		} else {
			event.type = ButtonRelease;
			eventMask = ButtonReleaseMask;
		}

		// which button
		unsigned int stateChangeMask = 0;
		if( buttonNumber == LEFT_BUTTON ) {
			event.xbutton.button = Button1;
			stateChangeMask = Button1Mask;
		} else if( buttonNumber == RIGHT_BUTTON ) {
			event.xbutton.button = Button3;
			stateChangeMask = Button3Mask;
		} else {
			logError("Unknown button number %i", buttonNumber);
		}

		X_CATCH_N_LOG if(!XSendEvent(dpy, PointerWindow, True, eventMask, &event)) {
			logError("Event could not be send");
		}

		returnDisplay(dpy);

		// update state (for following pointer move operations and button release event)
		if( buttonPressed ) {
			inputState |= stateChangeMask;
			ASSERT(inputState!=0);
		} else {
			inputState ^= stateChangeMask;
			ASSERT(inputState==0);
		}

		// control keys
		// unsigned int state = 0;
		// if (anEvent.isShift) state |= ShiftMask;
		// if (anEvent.isControl) state |= ControlMask;
		// if (anEvent.isAlt) state |= Mod1Mask;
		// if (anEvent.isMeta) state |= Mod4Mask;
		// http://www.doctort.org/adam/nerd-notes/x11-fake-keypress-event.html
		*/
	}
}


void keyboardAction(WindowId windowId, unsigned long keysym, Bool pressed) {
	// A key code corresponds roughly to a physical key, while a keysym corresponds to the symbol on the key top.
	// A list of KeySyms is associated with each KeyCode. Only key codes can be send to the X server.
	// Thus, even if the keysym says "Dollar-Sign" it still requires the shift modifier.
	// To explore keyboard mappings use xev.
	// how-to:
	// http://homepage3.nifty.com/tsato/xvkbd/events.html
	// http://hektor.umcs.lublin.pl/~mikosmul/computing/articles/custom-keyboard-layouts-xkb.html

	// TODO we need find a good keyboard mapping for all kinds of languages, or automatically switch mappings
	// TODO target window is ignored right now

	WindowId innerWindowId = getInnerWindowId(windowId);
	if (innerWindowId) {
		char *keysymString = XKeysymToString(keysym);
		logTrace("Requesting a keyboard input (keysym=%lu (%s), pressed=%i)", keysym, keysymString, pressed);

		// XK_Print saves a screenshot of the window (helpful to debug encoder code)
		if( keysym == XK_Print ) {
			logInfo("Taking a snapshot of the window %lu", windowId);
			WindowContent *wc = x11CaptureWindowContent(windowId);
			if( wc != NULL ) {
				writeImage(wc->imageDate, wc->dataSize, wc->width, wc->height, "print_window.bgra");
			}
			freeWindowContent(wc);
		}

		Display *dpy = getDisplay();

		KeyCode keycode = XKeysymToKeycode(dpy, keysym);
		if(keycode) {
			// XTestGrabControl (disp, True);
			X_CATCH_N_LOG( XTestFakeKeyEvent(dpy, keycode, pressed, 0) );
			// XTestGrabControl (disp, False);
		} else {
			logError("Unable to translate keysym %lu (%s) to keycode", keysym, keysymString);
		}

		returnDisplay(dpy);
	}
}

void requestWindowResize(WindowId windowId, int width, int height) {
	WindowId innerWindowId = getInnerWindowId(windowId);
	if (innerWindowId) {
		logTrace("Requesting a window resize (windowId=%lu, width=%i, height=%i)", windowId, width, height);
		Display *dpy = getDisplay();

		// swscale has a mod 8 size requirement
		// TODO increase width/height to a mod 8 size

		X_CATCH_N_LOG( XResizeWindow(dpy, innerWindowId, width, height) );

		returnDisplay(dpy);
	}
}


void requestWindowClose(WindowId windowId) {
	WindowId innerWindowId = getInnerWindowId(windowId);
	if (innerWindowId) {
		logDebug("Requesting a window close (windowId=%lu)", windowId);
		Display *dpy = getDisplay();

		XClientMessageEvent ev;
		memset(&ev, 0, sizeof (XClientMessageEvent));
		ev.type = ClientMessage;
		ev.window = windowId;
		ev.message_type = WM_PROTOCOLS;
		ev.format = 32;
		ev.data.l[0] = WM_DELETE_WINDOW;
		ev.data.l[1] = CurrentTime;
		X_CATCH_N_LOG( XSendEvent(dpy, windowId, False, NoEventMask, (XEvent*)&ev) );

		returnDisplay(dpy);
	}
}

