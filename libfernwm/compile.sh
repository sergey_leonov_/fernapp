#!/bin/bash

if [ -z "$JDK_HOME" ]; then
	echo "The JDK_HOME environment variable needs to point to your JDK."
	exit 1
fi

PROJECT_DIR=`pwd`
CC="gcc"
IDIRS="-I$PROJECT_DIR/src -I$JDK_HOME/include -I$JDK_HOME/include/linux `pkg-config --cflags gthread-2.0`"
LIBS="-lX11 -lXcomposite -lXdamage -lXtst -lpng `pkg-config --libs gthread-2.0`"
CFLAGS="-g -O0 -ggdb -Wall -std=gnu99 -funsigned-char -fPIC $IDIRS"
# use -O3 for better performance and -O0 for precise debugging
# -Werror is removed until h264 encoder and decoder use same avlib version again

echo Using compile flags: $CFLAGS
echo Using libraries: $LIBS

# names of source files without 'src/' prefix and without '.c' prefix
SRCS=`(cd src && find . -name "*.c" | sed s/\\\\.c//)`
TESTS=`(cd test && find . -name "*.c" | sed s/\\\\.c//)`
echo Sources are: $SRCS

echo Clean
rm -rf ./bin

echo Compiling
set -e
mkdir -p bin/util bin/encoders bin/x11connector bin/jni

for f in $SRCS; do
	(cd src && \
		$CC $CFLAGS -c ./$f.c -o ../bin/$f.o \
	)
done

echo Building shared library
$CC -shared $CFLAGS -o ./bin/libfernwm.so `find ./bin -name *.o` $LIBS

echo Executing unit tests
set -e; for TEST in $TESTS; do \
	echo Building unit test $TEST; \
	(cd test && $CC $CFLAGS -c ./$TEST.c -o $PROJECT_DIR/bin/$TEST.o -I ../src); \
	$CC $CFLAGS -o $PROJECT_DIR/bin/$TEST `find $PROJECT_DIR/bin -name *.o` $LIBS; \
	rm $PROJECT_DIR/bin/$TEST.o; \
	echo Running unit test $TEST; \
	(cd $PROJECT_DIR/bin && ./$TEST)
done
echo Unit tests completed successfully
