Changelog
=======

1.0.1
-------
 - Fixed compiling on 32-bit systems

1.0.0
-------
 - Application can be restarted if closed
 - Applet user agent will replace a closed main window with another
 - Removed the "useClassicX" server option
 - Renamed server option "port" to "tcpRaupPort"
 - Integrated analytics (can be disabled)
 - Fixed problems regarding applet not running with Java 8

0.6.0
-------
 - Support for running applications with a complex command line / argument list. Server options are given as JVM system properties now.
 - Improved documentation
 - Removed broken cross compile support. There is no more pre-compiled 32-bit binary. You need to compile from source if you have a 32-bit system.
 - Fixed race condition (no windows displayed in client)
 - Internal improvements

0.5.0
-------
 - fernapp-server.sh accepts application arguments now. Internally the application command is now added behind all other options and option "-app" has been removed. 
 - libfernwm can be compiled on 32-bit systems
 - made the port used by the internal HTTP web server configurable
 - fixed a bug that could cause a core dump (unmapped window didn't get correctly removed)
 - enabled core dumps as default in fernapp-server.sh
 - xterm is the default application now for internal tests
 - finished moving docs to wiki

0.4.2
-------
 - removed dependency on GLIBC-2.14

0.4.1
-------
 - fixed passphrase dialog

0.4.0
-------
 - support for java 6 and older glib
 - added site javadoc upload
 - fixed passphrase dialog


0.3.0
-------
 - support for 32-bit Linux


0.2.0
-------
 - switched to compile bash script for libfernwm
 - configured libfernwm to compile as a eclipse cdt project for linux
 - added libfernwm to maven build
 - removed h264 code from the project
 - maven based distribution build
 - made X display port is configurable
 - dev-run.sh executing in sub dir

 
