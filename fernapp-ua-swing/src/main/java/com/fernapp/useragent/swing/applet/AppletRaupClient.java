package com.fernapp.useragent.swing.applet;

import java.awt.Dimension;

import javax.annotation.concurrent.ThreadSafe;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.useragent.AbstractRaupClient;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.swing.applet.AppletUserAgent.AppletRemoteWindowContainer;
import com.fernapp.useragent.swing.ui.DecoratedRemoteWindow;
import com.fernapp.useragent.swing.ui.UndecoratedRemoteWindow;
import com.fernapp.useragent.ui.RemoteWindowController;
import com.fernapp.useragent.ui.RemoteWindowController.WindowState;
import com.fernapp.useragent.ui.WindowContainer;

/**
 * {@link RaupClient} implementation for a swing standalone user agent.
 * 
 * @author Markus
 */
@ThreadSafe
public class AppletRaupClient extends AbstractRaupClient {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private final AppletRemoteWindowContainer appletWindowContainer;
	private volatile Dimension browserWindowSize;
	private volatile RemoteWindowController mainWindow;

	public AppletRaupClient(AppletRemoteWindowContainer appletWindowContainer) {
		this.appletWindowContainer = appletWindowContainer;
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#createRemoteWindowController(com.fernapp.raup.windowmanagement.server.WindowSettings)
	 */
	@Override
	protected RemoteWindowController createRemoteWindowController(WindowSettings windowSettings) {
		if (mainWindow != null && mainWindow.getState() == RemoteWindowController.WindowState.CLOSED) {
			mainWindow = null;
		}

		if (mainWindow == null) {
			log.info("Displaying the new window as the main window");

			mainWindow = new RemoteWindowController(windowSettings.getWindowId(), appletWindowContainer, this);

			mainWindow.setWindowStateListener(new RemoteWindowController.WindowStateListener() {
				public void onStateChanged() {
					if (mainWindow.getState() == WindowState.CLOSED && getState() == State.CONNECTED) {
						replaceMainWindow();
					}
				}
			});

			if (browserWindowSize != null) {
				mainWindow.requestResize(browserWindowSize.width, browserWindowSize.height);
			} else {
				// browser size not yet known -> resize when setter is called
			}

			return mainWindow;

		} else {
			WindowContainer windowContainer;
			if (windowSettings.getParentWindowId() == null) {
				windowContainer = new DecoratedRemoteWindow();
			} else {
				windowContainer = new UndecoratedRemoteWindow();
			}

			return new RemoteWindowController(windowSettings.getWindowId(), windowContainer, this);
		}
	}

	public void replaceMainWindow() {
		mainWindow = null;

		RemoteWindowController newMainWindow = null;
		for (RemoteWindowController candidate : getWindows()) {
			if (candidate.getWindowSettings().getParentWindowId() == null) {
				newMainWindow = candidate;
			}
		}

		if (newMainWindow != null) {
			log.info("Making window " + newMainWindow.getWindowId() + " the new main window");
			WindowSettings windowSettings = newMainWindow.getWindowSettings();

			// remove existing window
			newMainWindow.dispose();
			windows.remove(newMainWindow.getWindowId());

			// and reopen it - just as if it was closed and reopened on the server
			onWindowSettingsChange(windowSettings);
		} else {
			log.info("No more open windows");
		}
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#showMessage(java.lang.String)
	 */
	@Override
	protected void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#getPassphrase()
	 */
	@Override
	protected String getPassphrase() {
		return JOptionPane.showInputDialog(appletWindowContainer.getAwtContainer(), "Enter passphrase:");
	}

	public synchronized void setBrowserWindowSize(Dimension browserWindowSize) {
		this.browserWindowSize = browserWindowSize;
		if (mainWindow != null) {
			mainWindow.requestResize(browserWindowSize.width, browserWindowSize.height);
		}
	}

}
