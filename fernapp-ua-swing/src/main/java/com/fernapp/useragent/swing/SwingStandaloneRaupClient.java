package com.fernapp.useragent.swing;

import javax.swing.JOptionPane;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.useragent.AbstractRaupClient;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.swing.ui.DecoratedRemoteWindow;
import com.fernapp.useragent.swing.ui.UndecoratedRemoteWindow;
import com.fernapp.useragent.ui.RemoteWindowController;
import com.fernapp.useragent.ui.WindowContainer;

/**
 * {@link RaupClient} implementation for a swing standalone user agent.
 * 
 * @author Markus
 */
public class SwingStandaloneRaupClient extends AbstractRaupClient {

	// private static final Logger log =
	// LoggerFactory.getLogger(SwingWindowManager.class);

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#createRemoteWindowController(com.fernapp.raup.windowmanagement.server.WindowSettings)
	 */
	@Override
	protected RemoteWindowController createRemoteWindowController(WindowSettings windowSettings) {
		WindowContainer windowContainer;
		if (windowSettings.getParentWindowId() == null) {
			windowContainer = new DecoratedRemoteWindow();
		} else {
			windowContainer = new UndecoratedRemoteWindow();
		}
		return new RemoteWindowController(windowSettings.getWindowId(), windowContainer, this);
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#showMessage(java.lang.String)
	 */
	@Override
	protected void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#getPassphrase()
	 */
	@Override
	protected String getPassphrase() {
		return JOptionPane.showInputDialog("Enter passphrase:");
	}

}
